# vincitysportiataymo

Vincity Sportia Tây Mỗ - Đại Mỗ

<a href="http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo"><strong>Vincity Sportia Tây Mỗ</strong></a> <strong>- Đại Mỗ đại đô thị Hạt Nhân thứ 2 ở Hà Nội mang thương hiệu Vincity của tập đoàn Vingroup được ra mắt vào năm 2018 sau dự án</strong> <a href="http://kenhbatdongsanviet.com/vincity-gia-lam"><strong>Vincity Ocean Park Gia Lâm</strong></a> <strong>. Đại đô thị thứ 2 này cũng được tích hợp những tiện ích vô cùng độc đáo hiện đại, những tiện ích khẳng định chưa từng có ở Việt Nam ngay trong lòng Hà Nội . Bom tấn tiếp theo của Vincity dự kiến sẽ làm dậy sóng thị trường bất động sản bờ Tây Hà Nội. </strong>

[caption id="attachment_5234" align="aligncenter" width="1100"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Phối-cảnh-tổng-quan-Vincity-Sporita-Tây-Mỗ.jpg"><img class="Phối cảnh tổng quan Vincity Sporita Tây Mỗ wp-image-5234 size-full" title="Phối cảnh tổng quan Vincity Sporita Tây Mỗ" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Phối-cảnh-tổng-quan-Vincity-Sporita-Tây-Mỗ.jpg" alt="Phối cảnh tổng quan Vincity Sporita Tây Mỗ" width="1100" height="525" /></a> Phối cảnh tổng quan Vincity Sporita Tây Mỗ[/caption]
<h2>TỔNG QUAN VINCITY SPORTIA TÂY MỖ</h2>
<div style="background-color: #15438a; border: 1px solid #4abaf7; border-radius: 1px; padding: 10px; width: auto;">

<span style="color: #ffffff;">Tên thương mại dự án:<a href="http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo ‎"> <strong>Vincity Sportia Tây Mỗ</strong></a>( <a href="http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo ‎"><strong>Vincity Tây Mỗ</strong></a> )</span>

<span style="color: #ffffff;">Chủ đầu tư : Công ty cổ phần tập đoàn Vingroup ( Tập Đoàn Vingroup )</span>

<span style="color: #ffffff;">Đơn vị thi công: Công ty cổ phần xây dựng Coteccons</span>

<span style="color: #ffffff;">Vị trí dự án : Thuộc địa phân quận Nam Từ Liềm, nằm trải dài trên trục đường  Đại Lộ Thăng Long , từ cầu vượt An Khánh đến nút giao thông Đại Học Đại Nam.</span>

<span style="color: #ffffff;">Tổng diện tích dự án: <strong><span style="color: #ff0000;">280ha</span></strong></span>

<span style="color: #ffffff;">Mật độ xây dựng :khoảng</span> <span style="color: #ff0000;"><strong>19%</strong></span>

<span style="color: #ffffff;">Đất trường học: khoảng 179.909m2</span>

<span style="color: #ffffff;">Diện tích đất nhà ở cao tầng: khoảng 273.475 m2</span>

<span style="color: #ffffff;">Đất nhà ở thấp tầng: khoảng 28.243 m2</span>

<span style="color: #ffffff;">Loại hình phát triển: </span>

<span style="color: #ffffff;">➕ Chia thành 5 phân khu:  The Dream, The Power, The Sun, The hero, the Victory </span>

<span style="color: #ffffff;">➕ Chung cư với khoảng <span style="color: #ff0000;"><strong>60</strong> </span>tòa tháp cao <span style="color: #ff0000;"><strong>35 </strong></span>đến <strong><span style="color: #ff0000;">39</span> </strong>tầng</span>

<span style="color: #ffffff;">➕ Biệt thự, liền kề, shophouse số lượng đang được cập nhật</span>

<span style="color: #ffffff;">➕ Khuôn viên cây xanh, dòng sông quanh dự án, hồ , bể bơi...</span>

<span style="color: #ffffff;">➕ Hồ điều hòa : <strong><span style="color: #ff0000;">7,3ha</span></strong></span>

<span style="color: #ffffff;">Thời gian khởi công : <span style="color: #ff0000;"><strong>2018</strong></span></span>

<span style="color: #ffffff;">Thời điểm bàn giao dự kiến : <span style="color: #ff0000;"><strong>2022.</strong></span></span>

</div>

<h2>VINCITY SPORTIA VỊ TRÍ ĐẮC ĐỊA</h2>
<div style="background-color: #bdf9f9; border: 1px solid #4abaf7; border-radius: 1px; padding: 10px; width: auto;">

<em><a href="http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo ‎"><strong>Vincity Sportia Tây Mỗ</strong></a> <span style="color: #003366;">- Đại Mỗ Hà Nội tọa lạc tại vị trí đắc địa nhất quận Nam Từ Liêm, trải dài theo dọc đường Đại Lộ Thăng Long danh tiếng. Thuộc địa phận 2 phường Tây Mỗ và phường Đại Mỗ quận Nam Từ Liêm.  Hưởng trọn vẹn toàn bộ cơ sở hạ tầng đồng bộ hoàn thiện của Quốc lộ 70 và Đại Lộ Thăng Long vô cùng thông thoáng thuận tiện. </span></em>

</div>

[caption id="attachment_5246" align="aligncenter" width="660"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Liên-kết-vùng-tiện-ích-tại-các-dự-án-Vincity.jpg"><img class="Liên kết vùng tiện ích tại các dự án Vincity wp-image-5246 size-full" title="Liên kết vùng tiện ích tại các dự án Vincity" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Liên-kết-vùng-tiện-ích-tại-các-dự-án-Vincity.jpg" alt="Liên kết vùng tiện ích tại các dự án Vincity" width="660" height="725" /></a> Liên kết vùng tiện ích tại các dự án Vincity[/caption]

<strong><span style="color: #ff9900;">➕</span> Phía Đông:</strong> giáp đường 70 m, sông Nhuệ và khu dân cư phường Đại Mỗ sầm uất.

<strong><span style="color: #ff9900;">➕</span> Phía Nam :</strong> giáp nhánh sông Cầu Triền, khu dân  cư phường Tây Mỗ, phường Đại Mỗ và một phần đường quy hoạch.

<strong><span style="color: #ff9900;">➕</span> Về phía Tây</strong> : giáp khu đô thị hai bên đường Lê Trọng Tấn .
<div style="background-color: #bdf9f9; border: 1px solid #4abaf7; border-radius: 1px; padding: 10px; width: auto;">

<span style="color: #003366;"><em>Kết nối giao thông vô cùng thuận tiện từ <a style="color: #003366;" href="http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo ‎"><strong>Vincity Sportia</strong></a> Tây Mỗ với mục tiêu nhằm tổ chức không gian kiến trúc cảnh quan 2 bên tuyến đường, tạo bộ mặt kiến trúc cho trục đường đô thị đáp ứng nhu cầu phát triển chung của khu vực, hài hòa với các công trình hiện có ở lân cận.</em></span>

</div>

[caption id="attachment_5245" align="aligncenter" width="960"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Đường-đua-F1-của-Vincity-Sporita-Tây-Mỗ.jpg"><img class="Đường đua F1 của Vincity Sporita Tây Mỗ wp-image-5245 size-full" title="Đường đua F1 của Vincity Sporita Tây Mỗ" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Đường-đua-F1-của-Vincity-Sporita-Tây-Mỗ.jpg" alt="Đường đua F1 của Vincity Sporita Tây Mỗ" width="960" height="841" /></a> Đường đua F1 của Vincity Sporita Tây Mỗ[/caption]

<span style="color: #ff9900;"><strong>➕</strong> </span>Cách BigC 4km

<strong><span style="color: #ff9900;">➕</span> </strong>Trung tâm hội nghị Quốc gia

<span style="color: #ff9900;"><strong>➕</strong> </span>Đại Siêu Thị Big C Thăng Long

<span style="color: #ff9900;"><strong>➕</strong> </span>Siêu dự án anh em Vinhomes Trần Duy Hưng De’capital

<span style="color: #ff9900;"><strong>➕</strong> </span>Trường VIP chuyên Hà Nội – Amstecdam

<span style="color: #ff9900;"><strong>➕</strong></span> Bảo Tàng Hà Nội

<span style="color: #ff9900;"><strong>➕</strong> </span>Khách sạn 5* JW Mariot

<span style="color: #ff9900;"><strong>➕</strong> </span>Khách sạn 7* Grand Plaza

&nbsp;

<h2>THIẾT KẾ VINCITY SPORTIA TÂY MỖ</h2>
[caption id="attachment_5275" align="aligncenter" width="1200"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Tổng-mặt-bằng-Vincity-Sportia-Tây-Mỗ-Đại-Mỗ.jpg"><img class="Tổng mặt bằng Vincity Sportia Tây Mỗ Đại Mỗ wp-image-5275 size-full" title="Tổng mặt bằng Vincity Sportia Tây Mỗ Đại Mỗ" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/11/Tổng-mặt-bằng-Vincity-Sportia-Tây-Mỗ-Đại-Mỗ.jpg" alt="Tổng mặt bằng Vincity Sportia Tây Mỗ Đại Mỗ" width="1200" height="848" /></a> Tổng mặt bằng Vincity Sportia Tây Mỗ Đại Mỗ[/caption]

<h2>TIỆN ÍCH TẠI VINCITY SPORTIA TÂY MỖ - ĐẠI MỖ</h2>
<span style="color: #ff9900;"><strong>➕</strong> </span>Phòng khám Vinmec đạt tiêu chuẩn quốc tế.

<span style="color: #ff9900;"><strong>➕</strong> </span>Hồ điều hòa: khoảng 7,33 ha

<span style="color: #ff9900;"><strong>➕</strong> </span>Trường học liên cấp quốc tế Vinshool hoặc tương đương.

<span style="color: #ff9900;"><strong>➕</strong> </span>Trung tâm thương mại Vincom và cửa hàng tiện ích.

<span style="color: #ff9900;"><strong>➕</strong> </span>Các tiện ích vui chơi giành riêng cho trẻ em.

<span style="color: #ff9900;"><strong>➕</strong> </span>Khu thể thao: bóng đá, tennis, bóng chuyền…

<span style="color: #ff9900;"><strong>➕</strong> </span>Khu công viên, phố đi dạo, bể bơi…

<span style="color: #ff9900;"><strong>➕</strong> </span>Cùng rất nhiều các tiện ích và dịch vụ khác



Nguồn Bài Viết : http://kenhbatdongsanviet.com/vincity-sportia-tay-mo-dai-mo